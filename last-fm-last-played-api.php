<?php
/**
 * Plugin Name: Last Listened Widget
 * Plugin URI: https://gitlab.com/sherman-house/wp-plugin---lastfm-last-listened-api
 * Description: Serves as a proxy for the LastFM API that
 * (hopefully) can't be spammed into going over my rate limit :)
 * Version: 1.0
 * Author: Phil Shannon
 * Author URI: https://philshannon.com
 * License: GPL v3
 * License URI: https://www.gnu.org/licenses/gpl-3.0.html
 * Text Domain: shermanhouse-last_listened
 * Domain Path: /languages
 */

require 'vendor/autoload.php';

use ShermanHouse\LastListenedWidget\Handler;

/**
 * Adds a menu item to the "Settings" menu in WP Admin Dashboard.
 */
\add_action('admin_menu', 'settings_page');

function settings_page() {
    add_submenu_page(
        'options-general.php',
        __('Last Listened Widget', 'shermanhouse-last_listened'),
        __('Last Listened Widget', 'shermanhouse-last_listened'),
        'manage_options',
        'last_listened',
        'last_listened_render_settings_page'
    );
}

/**
 * sets up the settings page
 */
function last_listened_render_settings_page() {
    ?>
    <div class="wrap">
        <h2><?php esc_html_e('Last Listened Widget Settings', 'shermanhouse-last_listened');?></h2>

        <form method="post" action="options.php">
            <?php
                settings_fields('last_listened_settings');
                do_settings_sections('last_listened_settings');
                submit_button();
            ?>
        </form>
    </div>
    <?php
}

/**
 * Creates settings
 */
\add_action('admin_init', 'last_listened_initialize_settings');
function last_listened_initialize_settings() {
    add_settings_section(
        'general_section',
        __('General Settings', 'shermanhouse-last_listened'),
        'general_settings_callback',
        'last_listened_settings'
    );
    add_settings_field(
        'api_key',
        __('LastFM API Key', 'shermanhouse-last_listened'),
        'text_input_callback',
        'last_listened_settings',
        'general_section',
        [
            'label_for' => 'api_key',
            'option_group' => 'last_listened_settings',
            'option_id' => 'api_key'
        ]
    );
    register_setting(
        'last_listened_settings',
        'last_listened_settings'
    );
}

/**
 * LastFM API Settings
 */
function general_settings_callback() {
    esc_html_e('Last Listened API Settings', 'shermanhouse-last_listened');
}

function text_input_callback($text_input) {
    // Get arguments from settings
    $option_group = $text_input['option_group'];
    $option_id = $text_input['option_id'];
    $option_name = "{$option_group}[{$option_id}]";
    $options = get_option($option_group);
    $option_value = $options[$option_id] ?? '';
    echo "<input type='text' size='50' id='{$option_id}' name='{$option_name}' value='{$option_value}'/>";
}


/**
 * EVERYTHING BELOW HERE IS FOR CUSTOM API ENDPOINTS
 */

/**
 * @param $request Options for the function.
 * @return string
 */
function get_user_currently_playing($request): ?string {
    $userName = $request->get_param('username');
    Handler::lastListenedHandler($userName);
}


\add_action('rest_api_init', 'register_last_fm_last_played_route');
function register_last_fm_last_played_route() {
    /**
     * the regex in this route might not capture every last.fm username.
     */
    register_rest_route(
        'last-fm-widget/v1',
        '/(?P<username>[a-zA-Z0-9_-]+)',
        [
            'methods' => 'GET',
            'callback' => 'get_user_currently_playing',
            'args' => [
                'id' => [
                    'sanitize_callback' => function($param, $request, $key) {
                        $trimmedParam = trim($param);
                        return filter_var( $trimmedParam, FILTER_SANITIZE_STRING);
                    }
                ]
            ]
        ]
    );
}