# LastFM Last Played API

LastFM Last Played API is a WordPress plugin. When activated, it adds an API endpoint to your WordPress API that returns metadata for the specified user's most recently played track. It is hopefully setup in a way that prevents anyone from pushing you over your rate limit.

## Installation

When in your wp-content/plugins directory, clone this repo:
```bash
git clone https://gitlab.com/sherman-house/wp-plugin---lastfm-last-listened-api.git
```
Then, from this repo's directory, install dependencies by running:
```bash
composer install
```
Open your WordPress Admin dashboard and go to your plugins. Activate this one.
Go to Settings->LastFM Last Played
Add your LastFM API key and save. You can get a LastFM API key from https://www.last.fm/api/account/create.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[MIT](https://choosealicense.com/licenses/mit/)