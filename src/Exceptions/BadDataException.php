<?php
namespace ShermanHouse\LastListenedWidget\Exceptions;

/**
 * I don't need any special functionality here; I just want a readable throw for my try/catch
 */
class BadDataException extends \Exception {

}