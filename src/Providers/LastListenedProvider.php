<?php

namespace ShermanHouse\LastListenedWidget\Providers;

use Exception;
use GuzzleHttp\Client as GuzzleClient;
use ShermanHouse\LastListenedWidget\Exceptions\BadDataException;
use ShermanHouse\LastListenedWidget\Providers\CacheProvider;
use ShermanHouse\LastListenedWidget\Models\LastListenedModel;

/**
 * This class is meant to serve as the sole place where we get data from the LastFM API.
 * Nothing else on the backend should know about LastFM. 
 */
class LastListenedProvider
{
    const BASE_URL = "http://ws.audioscrobbler.com/2.0";
    const SECONDS_TO_CACHE_LAST_FM_API_RESULTS = 5;

    /**
     * 
     * We'll have two entries in Redis for every request. We'll store the username and return that first.
     * However, since my LastFM API creds are based on requests/minute from a particular IP, I want to protect
     * myself from someone just spamming my endpoint with random LastFM usernames.
     * Therefore I'm also caching based on IP. It's not ideal if someone wants to get multiple usernames,
     * but they can make their own if they want that functionality. 
     * @throws BadDataException (when user configuration is bad)
     * @throws Exception (any other type of error)
     */
    public function hydrateLastListenedModel(LastListenedModel $lastListenedModel, string $userName): LastListenedModel {
        $options = \get_option('last_listened_settings') ?? null;
        $userIpAddress = $this->getIp();

        if (empty($options) || empty ($options['api_key'])) {
            throw new BadDataException('API key is not correctly configured in  plugin settings.');
        }
        $apiKey = $options['api_key'];

        // We could fail silently here and just always go straight to LastFM.
        // I don't want to do that just because I don't want any risk of hitting my rate limit,
        // so I'm not wrapping this in a try/catch to let the error bubble up.
        /** @var LastListenedModel $cachedModel */
        $cachedModel = json_decode((new CacheProvider())->get($userName));
        // TODO: Hydrate the model with this data. A direct conversion won't be possible
        if (empty($cachedModel) && !empty($userIpAddress)) {
            $cachedModel = (new CacheProvider())->get($userIpAddress);
        }

        if (!empty($cachedModel)) {
            $lastListenedModel = new LastListenedModel();
            $lastListenedModel->album = $cachedModel->album;
            $lastListenedModel->title = $cachedModel->title;
            $lastListenedModel->artist = $cachedModel->artist;
            $lastListenedModel->albumArtUrl = $cachedModel->albumArtUrl;
            $lastListenedModel->isCurrentlyListening = $cachedModel->isCurrentlyListening;
            $lastListenedModel->cacheHit = true;
            return $lastListenedModel;
        }

        // If we've gotten here, it means we have an API key configured and we don't have a cached model. Let's go to LastFM:
        // Again, no need to catch the exception here. We'll catch it higher up.
        $fullLastFmUrl = self::BASE_URL."?method=user.getrecenttracks&user=$userName&format=json&api_key=$apiKey&limit=1";
        $guzzleClient = new GuzzleClient();
        $lastFmResponse = $guzzleClient->request('GET', $fullLastFmUrl);
        $lastFmBody = json_decode($lastFmResponse->getBody());
        if (empty($lastFmBody) || empty($lastFmBody->recenttracks)) {
            throw new Exception('Not enough data received from LastFM to properly construct listening history.');
        }

        $lastPlayedTrack = $lastFmBody->recenttracks->track[0];

        $lastListenedModel->artist = $lastPlayedTrack->artist->{'#text'} ?? 'unknown';
        $lastListenedModel->title = $lastPlayedTrack->name ?? 'unknown';
        $lastListenedModel->album = $lastPlayedTrack->album->{'#text'} ?? 'unknown';
        $lastListenedModel->albumArtUrl = $this->getImageUrlBySize($lastPlayedTrack->image, "large");
        $lastListenedModel->isCurrentlyListening = $lastPlayedTrack->{'@attr'}->nowplaying ?? false;
        $lastListenedModel->cacheHit = false;

        // being very careful here to always set something if we can
        if (!empty($userIpAddress)) {
            try {
                (new CacheProvider())->set($userIpAddress, self::SECONDS_TO_CACHE_LAST_FM_API_RESULTS, json_encode($lastListenedModel));
            } catch (Exception $ex) {
                // Fail silently to client for now
                error_log($ex->getMessage());
            }
        }
        try {
            (new CacheProvider())->set($userName, self::SECONDS_TO_CACHE_LAST_FM_API_RESULTS, json_encode($lastListenedModel));
        } catch (Exception $ex) {
            error_log($ex->getMessage());
        }
        return $lastListenedModel;
    }

    private function getImageUrlBySize(array $images, string $imageSize): ?string {
        foreach ($images as $image) {
            if ($image->{'size'} === $imageSize) {
                return $image->{'#text'};
            }
        }
        return null;
    }

    /**
     * Shamelessly stolen from https://stackoverflow.com/questions/6717926/function-to-get-user-ip-address :)
     */
    private function getIP(): ?string {
        foreach (array('HTTP_CLIENT_IP', 'HTTP_X_FORWARDED_FOR', 'HTTP_X_FORWARDED', 'HTTP_X_CLUSTER_CLIENT_IP', 'HTTP_FORWARDED_FOR', 'HTTP_FORWARDED', 'REMOTE_ADDR') as $key) {
            if (array_key_exists($key, $_SERVER) === true) {
                foreach (array_map('trim', explode(',', $_SERVER[$key])) as $ip) {
                    if (filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_NO_PRIV_RANGE | FILTER_FLAG_NO_RES_RANGE) !== false) {
                        return $ip;
                    }
                }
            }
        }

        return null;
    }
}