<?php

namespace ShermanHouse\LastListenedWidget\Providers;

use Predis\Client;

/**
 * Currently this is a wrapper for super basic redis functionality.
 * It's in its own class so I don't have to worry about creating a new client in different places.
 */
class CacheProvider {
    
    /** @var Client */
    private $client;

    public function __construct()
    {
        $this->client = new Client([
            'scheme' => 'tcp',
            'host'   => 'redis', // This is set in Docker config. It is essentially a hostname for the redis instance running in the container.
            'port'   => 6379,
        ]);
    }

    /**
     * Returns null if there are no results. Otherwise, it's a string of the value.
     */
    public function get(string $key): ?string {
        return $this->client->get($key);
    }

    /**
     * I'm not setting a return type because Predis returns an int but it looks like Redis itself may return "OK"
     * @todo: Look into this more.
     */
    public function set(string $key, int $secondsTtl, string $value) {
        return $this->client->setex($key, $secondsTtl, $value);
    }
}