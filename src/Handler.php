<?php

namespace ShermanHouse\LastListenedWidget;

use ShermanHouse\LastListenedWidget\Exceptions\BadDataException;
use ShermanHouse\LastListenedWidget\Models\LastListenedModel;
use ShermanHouse\LastListenedWidget\Providers\LastListenedProvider;

class Handler {
    public static function lastListenedHandler(string $userName) {
        $lastListenedProvider = new LastListenedProvider();
        $errorObject = new \stdClass();
        try {
            $lastListenedModel = $lastListenedProvider->hydrateLastListenedModel(new LastListenedModel(), $userName);
            return \wp_send_json($lastListenedModel, 200);
        } catch (BadDataException $ex) {
            $errorObject->message = $ex->getMessage();
            $errorObject->statusCode = 401;
        } catch (\Exception $ex) {
            $errorObject->message = $ex->getMessage();
            $errorObject->statusCode = 500;
        }
        return \wp_send_json($errorObject, $errorObject->statusCode);
    }
}

