<?php

namespace ShermanHouse\LastListenedWidget\Models;

class LastListenedModel
{
    /** @var string */
    public $artist;
    /** @var string */
    public $album;
    /** @var string */
    public $title;
    /** @var string */
    public $albumArtUrl;
    /** @var bool */
    public $isCurrentlyListening;
    /** @var bool */
    public $cacheHit;

    public function getArtist(): string {
        return $this->artist;
    }
    public function setArtist(string $artist): void {
        $this->artist = $artist;
    }
    public function getAlbum(): string {
        return $this->album;
    }
    public function setAlbum(string $album): void {
        $this->album = $album;
    }
    public function getTitle(): string {
        return $this->title;
    }
    public function setTitle(string $title): void {
        $this->title = $title;
    }
    public function getAlbumArtUrl(): string {
        return $this->albumArtUrl;
    }
    public function setAlbumArtUrl(string $albumArtUrl): void {
        $this->albumArtUrl = $albumArtUrl;
    }
    public function getIsCurrentlyListening(): bool {
        return $this->isCurrentlyListening;
    }
    public function setIsCurrentlyListening(bool $isCurrentlyListening): void {
        $this->isCurrentlyListening = $isCurrentlyListening;
    }
    public function getCacheHit(): bool {
        return $this->cacheHit;
    }
}